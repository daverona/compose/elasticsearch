# daverona/docker-compose/elasticsearch

## Quick Start

```bash
cp .env.example .env  
# edit .env
docker-compose up --detach
```

```bash
docker container exec elasticsearch elasticsearch --version
```

## References

* Elasticsearch: [https://www.elastic.co/guide/en/elasticsearch/reference/current/getting-started.html](https://www.elastic.co/guide/en/elasticsearch/reference/current/getting-started.html)
* Elasticsearch repository: [https://github.com/elastic/elasticsearch](https://github.com/elastic/elasticsearch)
* Elasticsearch registry: [https://hub.docker.com/\_/elasticsearch](https://hub.docker.com/_/elasticsearch)
